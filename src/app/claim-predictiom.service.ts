import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';

@Injectable()
export class ClaimPredictionService {
  constructor(protected http: HttpClient) {
  }
  // tslint:disable-next-line:typedef
  predictClaim(claimData: ClaimPredictionPayload): Observable<any> {
    // return this.http.post('http://127.0.0.1:5000/claim-prediction', {
    //   'claim-amount': claimData.claimAmount,
    //   diagnosis: claimData.diagnosis,
    //   product: claimData.product,
    //   status: claimData.status
    // });
    return of (randomIntFromInterval());
    // return of(78);
  }
}

export interface ClaimPredictionPayload {
  claimAmount: number;
  diagnosis: string;
  product: string;
  status: string;
}

// tslint:disable-next-line:typedef
function randomIntFromInterval() {
  return Math.floor(Math.random() * (100 - 50 + 1) + 50);
}
