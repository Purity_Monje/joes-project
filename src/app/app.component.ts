import { Component } from '@angular/core';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {ClaimPredictionService} from './claim-predictiom.service';
import {Observable} from 'rxjs';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'claim-prediction';
  predictionScore = new Observable<any>();
  checkoutForm = this.formBuilder.group({
    claim_amount: new FormControl('', Validators.required),
    diagnosis: new FormControl('', Validators.required),
    product: new FormControl('', Validators.required),
    status: new FormControl('', Validators.required),
  });
  statuses = [
    {value: 'Approved', viewValue: 'Approved'},
    {value: 'Rejected', viewValue: 'Rejected'},
  ];
constructor(
    private claimPredictionService: ClaimPredictionService,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar
  ) {}

  onSubmit(): void {
    if (this.checkoutForm.valid) {
      this.claimPredictionService.predictClaim(this.checkoutForm.value).pipe(score => {
        return this.predictionScore = score;
      });
    } else {
      this.snackBar.open('Invalid form submitted', 'Close');
    }
    this.checkoutForm.reset();
  }
}
